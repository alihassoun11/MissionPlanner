# Readme de l'équipe VamUdeS

## Compilation

1. Installer Python 2.7 si ce n'est pas déjà fait
2. S'assurer que C:\Python27 est dans le PATH (Explorateur Windows -> Right-click sur "this PC" -> Properties -> Advanced System Settings -> Environment Variables -> Modifier "Path")
3. Redémarrer
4. Installer Visual Studio 2017 (ou 2015): https://visualstudio.microsoft.com/fr/vs/older-downloads/
5. Installer ".Net Core 2.0 development tools" et ".Net desktop development tools" dans la section modify du visual studio installer (voir: http://ardupilot.org/dev/docs/buildin-mission-planner.html)
6. Installer " mobile developement with .NET" encore dans la section modify du visual studio installer
7. Installer "dotnet core 2.2.108 - x86" (ou autre version 2.2 et plus, compatible avec VS 2017 ) -> https://dotnet.microsoft.com/download/dotnet-core/2.2
8. Cloner ce repo à l'aide d'un outil Git (On sugère GitKraken: https://www.gitkraken.com/pricing ) ( ça prend git: https://git-scm.com/downloads) et non par un zip
9. Ouvrir le fichier MissionPlanner.sln dans Visual Studio 2017 ou 2015 (version 2019 non compatible)
10. Right-click sur la solution -> Properties -> Configuration Properties-> Set Solution Configuration to "Debug" and Solution Platforms to x86 (menu deroulant)
11. In the Solutions Explorer, right-mouse-button click on Mission Planner and select Properties, Signing and uncheck “Sign the ClickOnce manifests”
12. Clique droit sur Mission Planner dans l'explorateur de solution -> Rebuild (peut prendre plusieurs essais)
13. Sélectionne le dossier Plugins dans l'explorateur de solution -> Clique droit -> Rebuild (peut prendre plusieurs essais)

## Troubleshoot:

- For .ddl errors : Ignore them, they are there because a projet referenced by another projet has errors.
- For erros in ExtLibs/ : Ignore them, they are not necessary for our usage of mission planner. You should not try to build any projects in ExtLibs/


## Références:

http://ardupilot.org/dev/docs/buildin-mission-planner.html

http://ardupilot.org/planner/docs/common-install-mission-planner.html

[![Gitter](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/ArduPilot/MissionPlanner?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

Website : http://ardupilot.org/planner/  
  
Forum : http://discuss.ardupilot.org/c/ground-control-software/mission-planner
  
Download latest stable version : http://firmware.ardupilot.org/Tools/MissionPlanner/MissionPlanner-latest.msi
  
Changelog : https://github.com/ArduPilot/MissionPlanner/blob/master/ChangeLog.txt  

License : https://github.com/ArduPilot/MissionPlanner/blob/master/COPYING.txt  


# Readme essentiel du MissionPlanner

[![Gitter](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/ArduPilot/MissionPlanner?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge) [![Build status](https://ci.appveyor.com/api/projects/status/2c5tbxr2wvcguihp?svg=true)](https://ci.appveyor.com/project/meee1/missionplanner)

Website : http://ardupilot.org/planner/  

Forum : http://discuss.ardupilot.org/c/ground-control-software/mission-planner

Download latest stable version : http://firmware.ardupilot.org/Tools/MissionPlanner/MissionPlanner-latest.msi

Changelog : https://github.com/ArduPilot/MissionPlanner/blob/master/ChangeLog.txt  

License : https://github.com/ArduPilot/MissionPlanner/blob/master/COPYING.txt  


## How to compile

### On Windows (Recommended)

#### 1. Install software

##### Main requirements

Currently, Mission Planner needs:

- Microsoft .NET Framework 4.6.1
- Microsoft .NET standard 2.0

##### IDE

###### Visual Studio Community
The recommended way to compile Mission Planner is through Visual Studio. You could do it with Visual Studio Community (version 16.9 or newer to include .NET standard 2.0) : [Visual Studio Download page](https://visualstudio.microsoft.com/downloads/ "Visual Studio Download page").
Visual Studio suite is quite complet and comes with Git support. On installation phase, please install support for :
- ASP.NET and web development
- .NET Desktop Developement
- Desktop development with C++
- Universal Windows Platform deveopment
- Mobile development with .NET
- Visual Studio extension development
- .NET Core cross-platofrm developemnt

###### VSCode
Currently VSCode with C# plugin is able to parse the code but cannot build.

#### 2. Get the code

If you get Visual Studio Community, you should be able to use Git from the IDE. 
Just clone `https://github.com/ArduPilot/MissionPlanner.git` to get the full code.

In case you didn't install an IDE, you will need to manually install Git. Please follow instruction in https://ardupilot.org/dev/docs/where-to-get-the-code.html#downloading-the-code-using-git

#### 3. Build

To build the code:
- Open MissionPlanner.sln with Visual Studio
- Compile just the MissionPlanner project

### On other systems
Building Mission Planner on other systems isn't support currently.

## Launching Mission Planner on other system

Mission Planner can be use with Mono on non-Windows system. 
Be aware that all the functionnalities aren't working.

### On Linux

#### Requirements

Those instructions were tested on Ubuntu 18.04.
Please install Mono, either :
- ` sudo apt install mono-runtime libmono-system-windows-forms4.0-cil libmono-system-core4.0-cil libmono-winforms4.0-cil libmono-corlib4.0-cil libmono-system-management4.0-cil libmono-system-xml-linq4.0-cil`

or full Mono :
- `sudo apt install mono-complete`

#### Launching

- Get the lastest zipped version of Mission Planner here : https://firmware.ardupilot.org/Tools/MissionPlanner/MissionPlanner-latest.zip
- Unzip in the directory you want
- Go into the directory
- run with `mono MissionPlanner.exe`

You can debug Mission Planner on Mono with `MONO_LOG_LEVEL=debug mono MissionPlanner.exe`


[![FlagCounter](https://s01.flagcounter.com/count2/A4bA/bg_FFFFFF/txt_000000/border_CCCCCC/columns_8/maxflags_40/viewers_0/labels_1/pageviews_0/flags_0/percent_0/)](https://info.flagcounter.com/A4bA)

